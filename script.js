//init
var intervalX = 1;
var intervalY = 1;
var fps = 30;

var draw = function() {
  var mainWindow = document.getElementById("message");  

  var rect = mainWindow.getBoundingClientRect();
  var myX = rect.left;
  var myY = rect.top;

  myX += intervalX;
  myY += intervalY;


  if ( (rect.right > window.innerWidth) || (rect.left < 0) ) {
    intervalX = intervalX * -1;
    myX += intervalX * 2;
  } 
  if ( (rect.bottom > window.innerHeight) || (rect.top < 0) ) {
    intervalY = intervalY * -1;
    myY += intervalY * 2;
  } 

  mainWindow.style.left = myX + "px";
  mainWindow.style.top = myY + "px";
}


setInterval(draw, 1000 / fps);
